package nl.patrickdevelopment.planningpoker.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import nl.patrickdevelopment.planningpoker.Model.User;
import nl.patrickdevelopment.planningpoker.R;

/**
 * Created by Aji on 31/03/16.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ContactViewHolder> {

    private User[] users;

    public UserAdapter( User[] users) {
        this.users = users;
    }

    @Override
    public int getItemCount() {
        if(users == null) {
            return 0;
        }
        return users.length;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        contactViewHolder.vName.setText(users[i].getName());
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.user_list_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected TextView vName;

        public ContactViewHolder(View v) {
            super(v);
            vName =  (TextView) v.findViewById(R.id.txtName);
        }
    }
}