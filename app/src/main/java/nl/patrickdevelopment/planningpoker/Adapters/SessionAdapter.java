package nl.patrickdevelopment.planningpoker.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.RelativeLayout;
import android.widget.TextView;

import nl.patrickdevelopment.planningpoker.Model.Session;
import nl.patrickdevelopment.planningpoker.R;

/**
 * Created by Patrick on 8-4-2016.
 */
public class SessionAdapter extends RecyclerView.Adapter<SessionAdapter.ContactViewHolder> {

    private Context context;
    private String session_code;
    private String session_date;


    private Session[] sessionList;

    /**
     * init taskadapter
     *
     * @param sessionList task list
     */
    public SessionAdapter(Session[] sessionList, Context context) {
        this.sessionList = sessionList;
        this.context = context;
    }


    @Override
    public int getItemCount() {
        if (sessionList == null) {
            return 0;
        }
        return sessionList.length;
    }

    /**
     * bind values to textviews
     *
     * @param contactViewHolder contactViewHolder
     * @param i                 current iteration number
     */
    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        session_code = context.getResources().getString(R.string.session_adapter_code);
        session_date = context.getResources().getString(R.string.session_adapter_date);

        contactViewHolder.TVSessionCode.setText(session_code + sessionList[i].getSession_code().toString());
        contactViewHolder.TVSessionDate.setText(session_date + sessionList[i].getCreation_date());

        //setFadeAnimation(contactViewHolder.linLayout);
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.session_list_item, viewGroup, false);
        return new ContactViewHolder(itemView);
    }

    /**
     * setup task item layout
     */
    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected RelativeLayout linLayout;
        protected TextView TVSessionCode;
        protected TextView TVSessionDate;

        public ContactViewHolder(View v) {
            super(v);
            linLayout = (RelativeLayout) v.findViewById(R.id.card_view);
            TVSessionCode = (TextView) v.findViewById(R.id.TVSession_Code);
            TVSessionDate = (TextView) v.findViewById(R.id.TVSession_date);
        }
    }

    /*private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }*/


}