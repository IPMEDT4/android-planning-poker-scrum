package nl.patrickdevelopment.planningpoker.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import nl.patrickdevelopment.planningpoker.Model.Task;
import nl.patrickdevelopment.planningpoker.R;

/**
 * Created by Aji on 31/03/16.
 */
public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ContactViewHolder> {

    private Task[] tasksList;

    /**
     * init taskadapter
     * @param tasksList task list
     */
    public TaskAdapter(Task[] tasksList) {
        this.tasksList = tasksList;
    }

    @Override
    public int getItemCount() {
        if(tasksList == null)
        {
            return 0;
        }
        return tasksList.length;
    }

    /**
     * bind values to textviews
     * @param contactViewHolder contactViewHolder
     * @param i current iteration number
     */
    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        contactViewHolder.vName.setText(tasksList[i].getDescription());
        String value = tasksList[i].getValue();
        switch (value){
            case "0":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_0);
                break;
            case "0.5":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_half);
                break;
            case "1":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_1);
                break;
            case "2":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_2);
                break;
            case "3":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_3);
                break;
            case "5":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_5);
                break;
            case "8":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_8);
                break;
            case "13":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_13);
                break;
            case "20":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_20);
                break;
            case "40":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_40);
                break;
            case "100":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_100);
                break;
            case "COFFEE":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_coffee);
                break;
            case "INFINITY":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_infinite);
                break;
            case "QUESTIONMARK":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_questionmark);
                break;
            case "":
                contactViewHolder.iCardValue.setImageResource(R.drawable.card_back);
                break;
        }
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.task_list_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    /**
     * setup task item layout
     */
    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected TextView vName;
        protected ImageView iCardValue;

        public ContactViewHolder(View v) {
            super(v);
            vName =  (TextView) v.findViewById(R.id.txtName);
            iCardValue = (ImageView) v.findViewById(R.id.valueIMG);
        }
    }
}