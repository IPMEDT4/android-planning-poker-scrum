package nl.patrickdevelopment.planningpoker.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import nl.patrickdevelopment.planningpoker.GameFragment;
import nl.patrickdevelopment.planningpoker.Model.User;
import nl.patrickdevelopment.planningpoker.R;

/**
 * Created by Michiel on 5-4-2016.
 */
public class UserValueAdapter extends RecyclerView.Adapter<UserValueAdapter.ContactViewHolder>{

    private User[] users;
    public UserValueAdapter( User[] users) {
        this.users = users;
    }

    @Override
    public int getItemCount() {
        if(users == null) {
            return 0;
        }
        return users.length;
    }

    public void clearData() {
        users = null;
    }


    @Override
    public void onBindViewHolder(ContactViewHolder contactViewHolder, int i) {
        contactViewHolder.vName.setText(users[i].getName());
        String value = users[i].getCardValue();
        if (value == null){
            contactViewHolder.iCardValue.setImageResource(R.drawable.card_back);
        } else {
            switch (value) {
                case "0":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_0);
                    break;
                case "0.5":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_half);
                    break;
                case "1":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_1);
                    break;
                case "2":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_2);
                    break;
                case "3":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_3);
                    break;
                case "5":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_5);
                    break;
                case "8":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_8);
                    break;
                case "13":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_13);
                    break;
                case "20":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_20);
                    break;
                case "40":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_40);
                    break;
                case "100":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_100);
                    break;
                case "COFFEE":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_coffee);
                    break;
                case "INFINITY":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_infinite);
                    break;
                case "QUESTIONMARK":
                    contactViewHolder.iCardValue.setImageResource(R.drawable.card_questionmark);
                    break;
            }
        }
        setFadeAnimation(contactViewHolder.linLayout);
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.
                from(viewGroup.getContext()).
                inflate(R.layout.game_list_item, viewGroup, false);

        return new ContactViewHolder(itemView);
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder {
        protected LinearLayout linLayout;
        protected TextView vName;
        protected ImageView iCardValue;

        public ContactViewHolder(View v) {
            super(v);
            linLayout = (LinearLayout) v.findViewById(R.id.linLayout);
            vName =  (TextView) v.findViewById(R.id.txtName);
            iCardValue = (ImageView) v.findViewById(R.id.imgCardValue);
        }

    }
    private void setFadeAnimation(View view) {
        AlphaAnimation anim = new AlphaAnimation(0.0f, 1.0f);
        anim.setDuration(500);
        view.startAnimation(anim);
    }

}
