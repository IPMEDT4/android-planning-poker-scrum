package nl.patrickdevelopment.planningpoker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Volley.JsonObjectPostRequest;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

public class JoinSessionActivity extends AppCompatActivity {

    public static EditText ETSessionCode;
    private ProgressDialog progress;
    private Button BSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_join_session);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //
        // get prefs
        //
        final SharedPreferences prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        //
        // setup button and add some color!
        //
        BSubmit = (Button) findViewById(R.id.bSessionID);
        BSubmit.setTextColor(getApplication().getResources().getColor(R.color.colorButtonText));
        ETSessionCode = (EditText) findViewById(R.id.etSessionID);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Change button to ripple for lollipop and above versions
            BSubmit.setBackgroundResource(R.drawable.ripple);
        } else{
            // do nothing for phones running an SDK before lollipop
        }

        //
        // onclick methods
        //
        ETSessionCode.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    BSubmit.performClick();
                    return true;
                }
                return false;
            }
        });

        BSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //check if code is empty
                if(ETSessionCode.getText().toString().isEmpty()) {
                    ETSessionCode.setError(getString(R.string.please_enter_session_code));
                }

                //check if user is set and session code is set
                if (prefs.getInt("user_id", 0) != 0 && ETSessionCode.getText().toString().length() > 0) {
                    //Session code is okay, join session.
                    VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());

                    progress = ProgressDialog.show(JoinSessionActivity.this, getString(R.string.progress_create_session_title),
                            getString(R.string.progress_create_session_message), true);

                    volleyHelper.setErrorListener(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progress.dismiss();
                            JoinSessionActivity.ETSessionCode.setError(getString(R.string.no_valid_session_code));
                        }
                    });

                    JsonObjectPostRequest request = volleyHelper.JoinSession(prefs.getInt("user_id", 0), Integer.parseInt(ETSessionCode.getText().toString()), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                //join the session
                                Intent game = new Intent(JoinSessionActivity.this, GameActivity.class);
                                game.putExtra("session_code", response.getInt("session_code"));
                                startActivity(game);
                                progress.dismiss();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    });
                    volleyHelper.addToRequestQueue(request);
                }
            }
        });
    }
}
