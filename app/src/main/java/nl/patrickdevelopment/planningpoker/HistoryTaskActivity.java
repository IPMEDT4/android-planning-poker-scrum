package nl.patrickdevelopment.planningpoker;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;

import org.json.JSONArray;

import nl.patrickdevelopment.planningpoker.Adapters.TaskAdapter;
import nl.patrickdevelopment.planningpoker.Model.Task;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

public class HistoryTaskActivity extends AppCompatActivity {

    private int Session_id;
    private final String STATE_SESSION_ID = "session_id";
    private Task[] taskList;
    private TaskAdapter taskAdapter;
    private RecyclerView RVTasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_task);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
            } else {
                Session_id = extras.getInt(STATE_SESSION_ID);
            }
        } else {
            Session_id = (int) savedInstanceState.getSerializable(STATE_SESSION_ID);
        }

        //
        // get settings
        //

        final SharedPreferences prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        //
        // get view stuff
        //
        RVTasks = (RecyclerView) findViewById(R.id.RVTasks);
        RVTasks.setHasFixedSize(true);
        RVTasks.setLayoutManager(new LinearLayoutManager(HistoryTaskActivity.this));

        //
        // get Tasks
        //
        VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());
        JsonArrayRequest tasks = volleyHelper.getTaskHistory(Session_id, prefs.getInt("user_id",0) , new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                taskList = null;
                Gson gson = new Gson();
                taskList = gson.fromJson(response.toString(), Task[].class);

                taskAdapter = new TaskAdapter(taskList);
                RVTasks.setAdapter(taskAdapter);

            }
        });
        volleyHelper.addToRequestQueue(tasks);
    }
}
