package nl.patrickdevelopment.planningpoker;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Response;

import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Model.Cards;
import nl.patrickdevelopment.planningpoker.Volley.JsonObjectPostRequest;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

public class GameActivity extends AppCompatActivity {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    private FloatingActionButton fab;
    private int Session_code;
    private boolean creator;

    static final String STATE_SESSION_CODE = "session_code";
    static final String STATE_CREATOR = "creator";

    private SharedPreferences prefs;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    /**
     * // Save the user's current game state
     *
     * @param outState bulde for save state
     */
    @Override
    protected void onSaveInstanceState(Bundle outState) {

        outState.putInt(STATE_SESSION_CODE, Session_code);
        outState.putBoolean(STATE_CREATOR, creator);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        //
        // setup prefs and session
        //
        prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                Session_code = 0;
                creator = false;
            } else {
                Session_code = extras.getInt(STATE_SESSION_CODE);
                creator = extras.getBoolean(STATE_CREATOR, false);
            }
        } else {
            Session_code = (int) savedInstanceState.getSerializable(STATE_SESSION_CODE);
            creator = (boolean) savedInstanceState.getSerializable(STATE_CREATOR);
        }


        //set session code in header
        setTitle("Planningpoker       "+getString(R.string.session_code)+": " + Session_code);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);

        //setup tabs
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        //add floating action button for to add tasks
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showEditDialog();
            }
        });

        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                //select tab
                tab.select();

                //show floating action button if first tab
                mViewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition() == 0) {
                    fab.show();
                } else {
                    fab.hide();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });


        //if not a creator go to game screen
        if(creator == false) {
            mViewPager.setCurrentItem(1);
            fab.hide();
        }

    }

    /**
     * press back button show diaglog if you really want to leave the game
     */
    @Override
    public void onBackPressed() {
        Log.d("back pressed", "if you happy and you know it clap your hands");
        // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.leaveGameMessage)
                .setTitle(R.string.leaveGame)
                .setPositiveButton("ja", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        //if the user is the creator end the session when leave
                        if (creator) {
                            VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());
                            JsonObjectPostRequest endSession = volleyHelper.endSession(prefs.getInt("user_id", 0), Session_code, new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    finish();
                                }
                            });
                            volleyHelper.addToRequestQueue(endSession);
                        } else {
                            finish();
                        }

                    }
                })
                .setNegativeButton(R.string.cancel, null);
        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    /**
     * show make task form
     */
    private void showEditDialog() {

        final SharedPreferences prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        FragmentManager fm = getSupportFragmentManager();
        TaskDialogFragment editNameDialog = TaskDialogFragment.newInstance(getString(R.string.create_task_title), this.Session_code, prefs.getInt("user_id", 0));
        editNameDialog.show(fm, "dialog");
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bundle bundle = new Bundle();
            bundle.putInt("session_code", Session_code);
            bundle.putBoolean("creator", creator);

            switch (position) {
                case 0:
                    TaskFragment TaskFragment = new TaskFragment();
                    TaskFragment.setArguments(bundle);
                    return TaskFragment;
                case 1:
                    GameFragment GameFragment = new GameFragment();
                    GameFragment.setArguments(bundle);
                    return GameFragment;
                case 2:

                    UserFragment UserFragment = new UserFragment();
                    UserFragment.setArguments(bundle);
                    return UserFragment;
            }

            return null;

            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.game_tab_title);
                case 1:
                    return getString(R.string.task_tab_title);
                case 2:
                    return getString(R.string.user_tab_title);
            }
            return null;
        }
    }

    /**
     * if card has been clicked send chosen card to server
     * @todo change place of this function to game fragment
     * @param view button click
     */
    public void clickImage(final View view) {
        Cards card = null;
        int id = 0;

        switch (view.getId()) {
            case R.id.card0:
                id = R.drawable.card_0;
                card = Cards.CARD_0;
                break;

            case R.id.cardHalf:
                id = R.drawable.card_half;
                card = Cards.CARD_05;
                break;

            case R.id.card1:
                id = R.drawable.card_1;
                card = Cards.CARD_1;
                break;

            case R.id.card2:
                id = R.drawable.card_2;
                card = Cards.CARD_2;
                break;

            case R.id.card3:
                id = R.drawable.card_3;
                card = Cards.CARD_3;
                break;

            case R.id.card5:
                id = R.drawable.card_5;
                card = Cards.CARD_5;
                break;

            case R.id.card8:
                id = R.drawable.card_8;
                card = Cards.CARD_8;
                break;

            case R.id.card13:
                id = R.drawable.card_13;
                card = Cards.CARD_13;
                break;

            case R.id.card20:
                id = R.drawable.card_20;
                card = Cards.CARD_20;
                break;

            case R.id.card40:
                id = R.drawable.card_40;
                card = Cards.CARD_40;
                break;

            case R.id.card100:
                id = R.drawable.card_100;
                card = Cards.CARD_100;
                break;

            case R.id.cardCoffee:
                id = R.drawable.card_coffee;
                card = Cards.CARD_COFFEE;
                break;

            case R.id.cardInfinity:
                id = R.drawable.card_infinite;
                card = Cards.CARD_INFINITY;
                break;

            case R.id.cardQuestionmark:
                id = R.drawable.card_questionmark;
                card = Cards.CARD_QUESTIONMARK;
                break;
        }


        final int finalId = id; //card drabable
        final Cards finalCard = card; //card enum object

        //set card drawable in imageview
        ImageView imageView = new ImageView(getApplicationContext());
        imageView.setImageResource(finalId);

        new AlertDialog.Builder(GameActivity.this)
                .setView(imageView, 0, 10, 0, 0)
                .setTitle(R.string.play_card_warning_title)
                .setMessage(R.string.play_card_warning_message)
                .setPositiveButton(R.string.play_card_warning_yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //change view to result screen
                        nextCard(view);

                        //send card to api
                        VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());
                        JsonObjectPostRequest request = volleyHelper.SetCardValue(prefs.getInt("user_id", 0), finalCard.toString(), Session_code, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("card", "send");
                            }
                        });

                        volleyHelper.addToRequestQueue(request);
                    }

                })
                .setNegativeButton(R.string.play_card_warning_no, null)
                .show();
    }

    /**
     * change the Visibility of the included views form cards to result
     * @param view image click view
     */
    public void nextCard(View view) {
        findViewById(R.id.iN_game_cards).setVisibility(View.GONE);
        findViewById(R.id.iN_game_results).setVisibility(View.VISIBLE);
    }
}
