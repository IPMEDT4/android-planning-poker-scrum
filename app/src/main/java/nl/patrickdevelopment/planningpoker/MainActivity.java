package nl.patrickdevelopment.planningpoker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.android.volley.Response;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONException;
import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Volley.JsonObjectPostRequest;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;
import nl.patrickdevelopment.planningpoker.gcloudMessage.RegistrationIntentService;

public class MainActivity extends AppCompatActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";

    private Button BJoinSession, BNewSession, BHistory;
    private ProgressDialog progress;
    VolleyHelper volleyHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //get prefs
        final SharedPreferences prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);
        volleyHelper =  VolleyHelper.getInstance(getApplicationContext());
        VolleyHelper.setDefaultDialogErrorListener(MainActivity.this);

        //
        //declare view objects
        //
        BJoinSession = (Button) findViewById(R.id.BJoinSession);
        BNewSession = (Button) findViewById(R.id.BNewSession);
        BHistory = (Button) findViewById(R.id.BHistory);

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Change button to ripple for lollipop and above versions
            BJoinSession.setBackgroundResource(R.drawable.ripple);
            BNewSession.setBackgroundResource(R.drawable.ripple);
            BHistory.setBackgroundResource(R.drawable.ripple);
        } else{
            // do nothing for phones running an SDK before lollipop
        }

        //
        //add some color
        //
        BJoinSession.setTextColor(getApplication().getResources().getColor(R.color.colorButtonText));
        BNewSession.setTextColor(getApplication().getResources().getColor(R.color.colorButtonText));
        BHistory.setTextColor(getApplication().getResources().getColor(R.color.colorButtonText));

        //
        //first startup
        //
        if (prefs.getBoolean("init", true) == true) {

            if (checkPlayServices()) {
                // Start IntentService to register this application with GCM.
                Intent intent = new Intent(this, RegistrationIntentService.class);
                startService(intent);
            }

            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
        }

        //
        //onclick methods
        //
        BJoinSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, JoinSessionActivity.class);
                startActivity(intent);
            }
        });


        BNewSession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //is user_id is created by server response
                if (prefs.getInt("user_id", 0) != 0) {

                    //show progress bar
                    progress = ProgressDialog.show(MainActivity.this, getString(R.string.progress_create_session_title),
                            getString(R.string.progress_create_session_message), true);

                    //@TODO hide progress dialog by error
                    JsonObjectPostRequest request = volleyHelper.CreateSession(prefs.getInt("user_id", 0), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //hide progress bar
                            progress.dismiss();
                            try {
                                //start new session
                                Intent game = new Intent(MainActivity.this, GameActivity.class);
                                game.putExtra("session_code", response.getInt("session_code"));
                                game.putExtra("creator", true);
                                startActivity(game);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    volleyHelper.addToRequestQueue(request);
                } else {
                    //user ID is not pressent

                    //show alert dialog
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    builder.setMessage(R.string.diaglog_no_user_id_message)
                            .setTitle(R.string.diaglog_no_user_id_title)
                            .setPositiveButton(R.string.diaglog_no_user_id_potive_button, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
                                    startActivity(intent);
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();

                }
            }
        });

        BHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, HistoryActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch(id) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                return true;
            case R.id.action_help:
                intent = new Intent(this, HelpActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
}


