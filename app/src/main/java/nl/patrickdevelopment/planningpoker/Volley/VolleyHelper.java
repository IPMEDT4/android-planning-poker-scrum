package nl.patrickdevelopment.planningpoker.Volley;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import nl.patrickdevelopment.planningpoker.R;

public class VolleyHelper {
    private static VolleyHelper mInstance;
    private RequestQueue mRequestQueue;
    private static Context mCtx;
    private static Activity CurrentActivity;
    private static Response.ErrorListener errorListener;

    //default url for api
    private final String BASE_URL = "https://api.ampapplications.nl";

    /**
     * constructor for volley
     *
     * @param context application context
     */
    public VolleyHelper(final Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();

        errorListener = new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response", "__" + error.getMessage());
            }
        };
    }

    /**
     * singleton handler
     *
     * @param context application context
     * @return Volleyhelper
     */
    public static synchronized VolleyHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new VolleyHelper(context);
        }
        return mInstance;
    }

    /**
     * get volley request queue
     * @return RequestQueue
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    /**
     * add volley request object to queue
     * @param req requestObject
     * @param <T> requestObject
     */
    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    /**
     * create user volley request
     * @param userName user name
     * @param GCMToken google cloud messaging token
     * @param listener response listener
     * @return JsonObjectPostRequest
     */
    public JsonObjectPostRequest createUser(String userName, String GCMToken, Response.Listener<JSONObject> listener) {
        Map<String, String> params;
        // POST params to be sent to the server
        params = new HashMap<String, String>();
        params.put("name", userName);
        params.put("gcm-token", GCMToken);

        JsonObjectPostRequest postRequest = new JsonObjectPostRequest(Request.Method.POST, BASE_URL + "/user", params,
                listener,
                this.errorListener()
        );
        return postRequest;
    }

    /**
     * create session
     * @param user_id user id
     * @param listener Listener
     * @return JsonObjectPostRequest
     */
    public JsonObjectPostRequest CreateSession(Integer user_id, Response.Listener<JSONObject> listener) {

        Map<String, String> params;
        // POST params to be sent to the server
        params = new HashMap<String, String>();
        params.put("user_id", user_id.toString());

        JsonObjectPostRequest postRequest = new JsonObjectPostRequest(Request.Method.POST, BASE_URL + "/session", params,
                listener,
                this.errorListener()
        );
        return postRequest;
    }

    /**
     * creat task by session code
     * @param user_id user id
     * @param session_code session code
     * @param description task description
     * @param listener Response Listener
     * @return JsonObjectPostRequest
     */
    public JsonObjectPostRequest CreateTask(Integer user_id, Integer session_code, String description, Response.Listener<JSONObject> listener) {
        Map<String, String> params;
        // POST params to be sent to the server
        params = new HashMap<String, String>();
        params.put("user_id", user_id.toString());
        params.put("session_code", session_code.toString());
        params.put("description", description);

        JsonObjectPostRequest postRequest = new JsonObjectPostRequest(Request.Method.POST, BASE_URL + "/task", params,
                listener,
                this.errorListener()
        );
        return postRequest;
    }


    /**
     * get task by session code
     * @param session_code session code
     * @param user_id user id
     * @param listener Response Listener
     * @return JsonArrayRequest
     */
    public JsonArrayRequest GetTasks(Integer session_code, Integer user_id, Response.Listener<JSONArray> listener) {

        JsonArrayRequest getTasks = new JsonArrayRequest(
                Request.Method.GET,
                BASE_URL + "/task/" + session_code + "/" + user_id,
                listener,
                this.errorListener());
        return getTasks;
    }

    /**
     * get session information by session code
     * @param session_code session code
     * @param user_id user id
     * @param listener listener
     * @return JsonObjectRequest
     */
    public JsonObjectRequest GetSession(Integer session_code, Integer user_id, Response.Listener<JSONObject> listener) {

        JsonObjectRequest getSession = new JsonObjectRequest(
                Request.Method.GET,
                BASE_URL + "/session/" + session_code + "?user_id=" + user_id,
                listener,
                this.errorListener());
        return getSession;
    }

    /**
     * get users by session code
     * @param session_code session code
     * @param listener listener
     * @return JsonArrayRequest
     */
    public JsonArrayRequest getUserBySession(int session_code, int user_id, Response.Listener<JSONArray> listener) {
        return new JsonArrayRequest(BASE_URL + "/session/" + session_code + "/users?user_id=" + user_id, listener, this.errorListener());
    }

    /**
     * join an online session
     * @param user_id      unique user id
     * @param session_code unique sesion code
     * @param listener     response listener
     * @return JsonObjectPostRequest
     */
    public JsonObjectPostRequest JoinSession(Integer user_id, Integer session_code, Response.Listener<JSONObject> listener) {

        Map<String, String> params;
        // POST params to be sent to the server
        params = new HashMap<String, String>();
        params.put("user_id", user_id.toString());
        params.put("session_code", session_code.toString());

        JsonObjectPostRequest postRequest = new JsonObjectPostRequest(Request.Method.POST, BASE_URL + "/session/" + session_code, params,
                listener,
                this.errorListener()
        );
        return postRequest;
    }

    /**
     * set card value post by session code
     * @param user_id
     * @param card
     * @param session_code
     * @param listener
     * @return JsonObjectPostRequest
     */
    public JsonObjectPostRequest SetCardValue(Integer user_id, String card, Integer session_code, Response.Listener<JSONObject> listener) {
        Map<String, String> params;
        // POST params to be sent to the server
        params = new HashMap<String, String>();
        params.put("user_id", user_id.toString());
        params.put("card", card);
        params.put("session_code", session_code.toString());

        JsonObjectPostRequest postRequest = new JsonObjectPostRequest(Request.Method.POST, BASE_URL + "/task/card", params,
                listener,
                errorListener()
        );
        return postRequest;
    }

    /**
     * next task string request for volley
     * @param user_id      user id number
     * @param session_code session code
     * @param listener     response listener
     * @return StringRequest
     */
    public StringRequest nextTask(Integer user_id, Integer session_code, Response.Listener<String> listener) {
        return new StringRequest(Request.Method.POST, BASE_URL + "/task/next?user_id=" + user_id + "&session_code=" + session_code, listener, this.errorListener());
    }

    /**
     * replay task string request for volley
     * @param user_id      user id number
     * @param session_code session code
     * @param listener     response listener
     * @return StringRequest
     */
    public StringRequest replayTask(Integer user_id, Integer session_code, Response.Listener<String> listener) {
        return new StringRequest(Request.Method.POST, BASE_URL + "/task/reset?user_id=" + user_id + "&session_code=" + session_code, listener, this.errorListener());
    }

    /**
     * end session string request for volley
     *
     * @param user_id      user id number
     * @param session_code session code
     * @param listener     response listener
     * @return JsonObjectPostRequest
     */
    public JsonObjectPostRequest endSession(Integer user_id, Integer session_code, Response.Listener<JSONObject> listener) {
        Map<String, String> params;
        // POST params to be sent to the server
        params = new HashMap<String, String>();
        params.put("user_id", user_id.toString());

        JsonObjectPostRequest postRequest = new JsonObjectPostRequest(Request.Method.POST, BASE_URL + "/session/end/" + session_code, params,
                listener,
                errorListener()
        );
        return postRequest;
    }

    public JsonObjectRequest getHistory(Integer user_id, Response.Listener<JSONObject> listener) {

        JsonObjectRequest getRequest = new JsonObjectRequest(BASE_URL + "/user/session/"+ user_id,
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error",error.toString());
                    }
                }
        );
        return getRequest;
    }

    /**
     * get task session hisotory by session ID
     * @param session_id session id
     * @param user_id user id
     * @param listener listener
     * @return
     */
    public JsonArrayRequest getTaskHistory(Integer session_id, Integer user_id, Response.Listener<JSONArray> listener) {

        JsonArrayRequest getRequest = new JsonArrayRequest(BASE_URL + "/task/sessionid/"+ session_id  +"?user_id="+ user_id,
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("error",error.toString());
                    }
                }
        );
        return getRequest;
    }

    /**
     * set an error message with een dialog
     * @param CurrentActivity current activity
     */
    public static void setDefaultDialogErrorListener(Activity CurrentActivity) {
        VolleyHelper.CurrentActivity = CurrentActivity;
        VolleyHelper.setErrorListener(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("volley", "error");
                // 1. Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder builder = new AlertDialog.Builder(VolleyHelper.CurrentActivity);

                // 2. Chain together various setter methods to set the dialog characteristics
                builder.setMessage(R.string.volley_response_error_message)
                        .setTitle(R.string.volley_response_error_title)
                        .setPositiveButton(R.string.volley_response_error_positive_button, null);


                // 3. Get the AlertDialog from create()
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });
    }


    /**
     * get error listener
     * @return ErrorListener ErrorListener
     */
    private Response.ErrorListener errorListener() {
        return errorListener;
    }

    /**
     * set error listener
     * @param errorListener errorListener
     */
    public static void setErrorListener(Response.ErrorListener errorListener) {
        VolleyHelper.errorListener = errorListener;
    }

}
