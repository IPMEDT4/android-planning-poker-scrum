package nl.patrickdevelopment.planningpoker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import nl.patrickdevelopment.planningpoker.Adapters.TaskAdapter;
import nl.patrickdevelopment.planningpoker.Model.Task;
import nl.patrickdevelopment.planningpoker.Model.User;
import nl.patrickdevelopment.planningpoker.Volley.JsonObjectPostRequest;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

/**
 * Created by Patrick on 22-3-2016.
 */
public class TaskFragment extends Fragment {

    View viewPos;
    private RecyclerView rv_tasks;
    private TaskAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    protected Task[] tasksList;
    private int session_code;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_task, container, false);

        //
        // get prefs and volley
        //
        final SharedPreferences prefs = getActivity().getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        VolleyHelper volleyHelper = VolleyHelper.getInstance(getContext());

        //
        // setup view elements
        //
        session_code = this.getArguments().getInt("session_code");
        viewPos = rootView.findViewById(R.id.cordLayout);
        rv_tasks = (RecyclerView)rootView.findViewById(R.id.rv_tasks);

        rv_tasks.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        rv_tasks.setLayoutManager(mLayoutManager);

        //setup broadcast receiver
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("session-update-event"));


        //get all tasks
        JsonArrayRequest request = volleyHelper.GetTasks(session_code, prefs.getInt("user_id",0) , new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.d("task", "created request");
                Gson gson = new Gson();
                tasksList = gson.fromJson(response.toString(), Task[].class);
                updateList();

            }
        });
        volleyHelper.addToRequestQueue(request);

        return rootView;
    }

    /**
     * update tasklist
     */
    private void updateList() {
        mAdapter = new TaskAdapter(tasksList);
        rv_tasks.setAdapter(mAdapter);
    }


    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("session");

            //refresh list on message receive
            try {
                JSONObject json = new JSONObject(message);
                tasksList = null;

                Gson gson = new Gson();
                tasksList = gson.fromJson(json.getJSONArray("task").toString(), Task[].class);
                updateList();


            } catch (JSONException e) {
                e.printStackTrace();
            }

            Log.d("receiver", "Got message: " + message);
        }
    };

    //@TODO update task
    //@TODO delete task
}
