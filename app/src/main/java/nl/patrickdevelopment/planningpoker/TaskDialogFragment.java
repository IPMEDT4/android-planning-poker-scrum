package nl.patrickdevelopment.planningpoker;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Response;

import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Volley.JsonObjectPostRequest;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

/**
 * Created by Patrick on 29-3-2016.
 *
 * source: https://github.com/codepath/android_guides/wiki/Using-DialogFragment
 * http://developer.android.com/guide/topics/ui/dialogs.html
 */
public class TaskDialogFragment extends DialogFragment {

    private EditText ETTaskDescription;
    private Button BSave;
    private Button BDismiss;
    private Integer session_code = null;
    private Integer user_id = null;

    public TaskDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    /**
     * setup diaglog fragment
     * @param title dialog title
     * @param session_code session_code
     * @param user_id user id
     * @return TaskDialogFragment
     */
    public static TaskDialogFragment newInstance(String title,int session_code,int user_id) {
        TaskDialogFragment frag = new TaskDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putInt("session_code", session_code);
        args.putInt("user_id", user_id);
        frag.setArguments(args);
        return frag;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_task_dialog, container);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // Get field from view
        ETTaskDescription = (EditText) view.findViewById(R.id.ETTaskDescription);
        BSave = (Button) view.findViewById(R.id.BSave);
        BDismiss = (Button) view.findViewById(R.id.BDismiss);

        // Fetch arguments from bundle and set title
        String title = getArguments().getString("title", "Enter Name");
        session_code = getArguments().getInt("session_code", 0);
        user_id = getArguments().getInt("user_id", 0);

        getDialog().setTitle(title);
        // Show soft keyboard automatically and request focus to field
        ETTaskDescription.requestFocus();
        getDialog().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        BDismiss.setOnClickListener((new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getActivity().getSupportFragmentManager().beginTransaction().remove(TaskDialogFragment.this).commit();
            }
        }));

        //set button listener
        BSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("button","create task pressed");
                Log.d("session_code",String.valueOf(session_code));
                Log.d("user_id",String.valueOf(user_id));
                String description = ETTaskDescription.getText().toString();

                //create task on server
                if(session_code != 0 && user_id != 0) {
                    if(description.matches("")){
                        Toast.makeText(getActivity(), getContext().getResources().getString(R.string.task_dialog_error), Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        VolleyHelper volleyHelper = VolleyHelper.getInstance(getContext());
                        //save new task on server
                        JsonObjectPostRequest request = volleyHelper.CreateTask(user_id, session_code, description, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                //close fragment
                                getActivity().getSupportFragmentManager().beginTransaction().remove(TaskDialogFragment.this).commit();
                            }
                        });
                        volleyHelper.addToRequestQueue(request);
                    }
                }
            }
        });
    }
}