package nl.patrickdevelopment.planningpoker.Model;

/**
 * Created by Patrick on 17-3-2016.
 */
public enum Cards {
    CARD_0("0"),
    CARD_05("0.5"),
    CARD_1("1"),
    CARD_2("2"),
    CARD_3("3"),
    CARD_5("5"),
    CARD_8("8"),
    CARD_13("13"),
    CARD_20("20"),
    CARD_40("40"),
    CARD_100("100"),
    CARD_COFFEE("COFFEE"),
    CARD_INFINITY("INFINITY"),
    CARD_QUESTIONMARK("QUESTIONMARK");


    private String cardValue;

    Cards(String cardValue) {
        this.cardValue = cardValue;
    }
    @Override
    public String toString() {
        // you can localise this string somehow here
        return cardValue;
    }
}
