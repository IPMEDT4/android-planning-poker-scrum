package nl.patrickdevelopment.planningpoker.Model;

/**
 * Created by Patrick on 17-3-2016.
 */
public class Task {

    public Integer id;
    public String description;
    public String value;

    public Task(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
