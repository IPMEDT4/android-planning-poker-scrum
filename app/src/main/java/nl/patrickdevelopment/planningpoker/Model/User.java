package nl.patrickdevelopment.planningpoker.Model;

/**
 * Created by Patrick on 17-3-2016.
 */
public class User {

    public Integer id;
    public String name;
    public Rols rol;
    public String GCMToken;
    public String card;

    public User(String name) {
        this.name = name;
    }

    public User(Integer ID, String name) {
        this.id = ID;
        this.name = name;
    }

    public String getGCMToken() {
        return GCMToken;
    }

    public void setGCMToken(String GCMToken) {
        this.GCMToken = GCMToken;
    }

    public Rols getRol() {
        return rol;
    }

    public void setRol(Rols rol) {
        this.rol = rol;
    }

    public Integer getID() {
        return id;
    }

    public void setID(Integer ID) {
        this.id = ID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public String getCardValue() {
        return card;
    }

    public void setCardValue(String card) {
        this.card = card;
    }
}
