package nl.patrickdevelopment.planningpoker.Model;

/**
 * Created by Patrick on 17-3-2016.
 */
public class Session {

    public Integer id;
    public Integer session_code;
    public String created_at;

    public Integer getSession_code() {
        return session_code;
    }

    public void setSession_code(Integer session_code) {
        this.session_code = session_code;
    }

    public String getCreation_date() {
        return created_at;
    }

    public void setCreation_date(String creation_date) {
        this.created_at = creation_date;
    }

    public Session(Integer ID) {
        this.id = ID;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer ID) {
        this.id = ID;
    }

}
