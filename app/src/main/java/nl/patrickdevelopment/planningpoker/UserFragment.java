package nl.patrickdevelopment.planningpoker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Adapters.UserAdapter;
import nl.patrickdevelopment.planningpoker.Model.User;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

/**
 * Created by Patrick on 22-3-2016.
 */
public class UserFragment extends Fragment {

    private RecyclerView RVUsers;
    private UserAdapter userAdapter;
    protected static User[] usersList;
    private int sessionCode;
    private SharedPreferences prefs;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_user, container, false);

        //
        // get prefs and set session code
        //
        prefs = getActivity().getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        sessionCode = this.getArguments().getInt("session_code");

        //
        // setup broadcast receiver
        //
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("session-update-event"));

        //
        // setup layout
        //
        RVUsers = (RecyclerView)rootView.findViewById(R.id.RVUsers);

        RVUsers.setHasFixedSize(true);
        RVUsers.setLayoutManager(new LinearLayoutManager(getActivity()));


        //get users from server when you join or start the session
        VolleyHelper volleyHelper = new VolleyHelper(getContext());
        JsonArrayRequest getUsers = volleyHelper.getUserBySession(sessionCode, prefs.getInt("user_id", 0), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Gson gson = new Gson();
                usersList = gson.fromJson(response.toString().toString(), User[].class);
                updateList();
                Log.e("User", response.toString());
            }
        });
        volleyHelper.addToRequestQueue(getUsers);


        return rootView;
    }

    /**
     * update list
     */
    private void updateList() {
        userAdapter = new UserAdapter(usersList);
        RVUsers.setAdapter(userAdapter);
    }

    /**
     * receive google cloud messages
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("session");

            try {
                JSONObject json = new JSONObject(message);
                usersList = null;

                Gson gson = new Gson();
                usersList = gson.fromJson(json.getJSONArray("user").toString(), User[].class);
                updateList();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };
}
