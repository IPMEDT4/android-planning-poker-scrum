package nl.patrickdevelopment.planningpoker;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import nl.patrickdevelopment.planningpoker.Adapters.UserValueAdapter;
import nl.patrickdevelopment.planningpoker.Model.Task;
import nl.patrickdevelopment.planningpoker.Model.User;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

/**
 * Created by Patrick on 22-3-2016.
 */
public class GameFragment extends Fragment {

    private Button BNextTask, BTaskReplay, BStartGame;
    private View IN_game_results, IN_game_cards, IN_game_start;
    private TextView TVResultWaitText, currentTask, TVSessieCode;
    private RelativeLayout RLButtonPannel;
    private SharedPreferences prefs;

    protected static User[] usersList;
    private UserValueAdapter userValueAdapter;
    private RecyclerView RVUsers;

    private int session_code;
    private Boolean creator;
    private Task[] sessionList;
    private VolleyHelper volleyHelper;
    String nextTaskString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);

        //get values
        prefs = getActivity().getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);
        session_code = this.getArguments().getInt("session_code");
        creator = this.getArguments().getBoolean("creator");
        volleyHelper = VolleyHelper.getInstance(getContext());

        //
        // start start listen to google cloud messages
        //
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(mMessageReceiver,
                new IntentFilter("session-update-event"));

        //
        //set layout stuff
        //
        nextTaskString = getResources().getString(R.string.current_task);
        BNextTask = (Button) rootView.findViewById(R.id.BNextTask);
        BTaskReplay = (Button) rootView.findViewById(R.id.BTaskReplay);

        IN_game_results = rootView.findViewById(R.id.iN_game_results);
        IN_game_cards = rootView.findViewById(R.id.iN_game_cards);
        IN_game_start = rootView.findViewById(R.id.iN_game_start);

        BNextTask = (Button) IN_game_results.findViewById(R.id.BNextTask);
        BTaskReplay = (Button) IN_game_results.findViewById(R.id.BTaskReplay);
        BStartGame = (Button) IN_game_start.findViewById(R.id.BStartGame);
        RVUsers = (RecyclerView) rootView.findViewById(R.id.RVUsers);
        currentTask = (TextView) rootView.findViewById(R.id.currentTask);
        RLButtonPannel = (RelativeLayout) IN_game_results.findViewById(R.id.buttonPanel);
        TVSessieCode = (TextView) IN_game_start.findViewById(R.id.TVsessieCode);


        //
        // add some color
        //
        BNextTask.setTextColor(Color.WHITE);
        BTaskReplay.setTextColor(Color.WHITE);
        BStartGame.setTextColor(Color.WHITE);
        TVSessieCode.setTextColor(getResources().getColor(R.color.colorPrimary));
        //set sessiecode in textview
        TVSessieCode.setText(String.valueOf(session_code));



        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            // Change button to ripple for lollipop and above versions
            BNextTask.setBackgroundResource(R.drawable.ripple);
            BTaskReplay.setBackgroundResource(R.drawable.ripple);
            BStartGame.setBackgroundResource(R.drawable.ripple);
        } else {
            // do nothing for phones running an SDK before lollipop
        }

        //
        //setup listview
        //
        RVUsers.setHasFixedSize(true);
        RVUsers.setLayoutManager(new LinearLayoutManager(getActivity()));

        //
        // get current tasks
        //

        JsonObjectRequest request = volleyHelper.GetSession(session_code, prefs.getInt("user_id", 0), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                ProcessCurrentTask(response);
            }
        });
        volleyHelper.addToRequestQueue(request);

        //
        // set on click listeners
        //
        //start game click listener
        BStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest nextTask = volleyHelper.nextTask(prefs.getInt("user_id", 0), session_code, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response.toString());
                        try {
                            JSONObject json = new JSONObject(response.toString());
                            if (json.getString("status").equals("Next task asignt")) {
                                if (currentTask.length() > 0) {
                                    //if next task is assign show wait text in the result fragment and clear the result list
                                    userValueAdapter.clearData();
                                }
                            } else {
                                Log.d("next task", "no more tasks, please add one");
                                //@todo give error there is no more tasks
                                new AlertDialog.Builder(getContext())
                                        .setTitle(R.string.no_task_error_title)
                                        .setMessage(R.string.no_task_error_message)
                                        .setNeutralButton(R.string.no_task_error_button, null)
                                        .show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                volleyHelper.addToRequestQueue(nextTask);
            }
        });
        //next task click listener
        BNextTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nextTask();
            }
        });

        //task replay click listener
        BTaskReplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                StringRequest nextTask = volleyHelper.replayTask(prefs.getInt("user_id", 0), session_code, new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response.toString());
                        userValueAdapter.clearData();
                    }
                });
                volleyHelper.addToRequestQueue(nextTask);
            }
        });

        //if not creator hide buttons for next en replay task
        if (creator == false) {
            RLButtonPannel.setVisibility(View.GONE);
            BStartGame.setVisibility(View.GONE);
        }

        showCards();

        return rootView;
    }

    /**
     * process curren task object for set current task
     *
     * @param json
     */
    private void ProcessCurrentTask(JSONObject json) {
        //if is an current task
        if (!json.isNull("current_task")) {
            try {
                JSONObject currentTaskJson = json.getJSONObject("current_task");
                //loop true array for description
                Iterator<String> iter = currentTaskJson.keys();
                while (iter.hasNext()) {
                    String key = iter.next();
                    try {
                        Object value = currentTaskJson.get(key);
                        if (key.equals("description")) {
                            //if description is not null set description
                            if (value.toString() != null) {
                                currentTask.setText(nextTaskString+ "      " + value.toString());
                                //if no description show cards
                            } else {
                                showCards();
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            startGame();
        }
    }

    /**
     * show cards include and hide other includes
     */
    private void showCards() {
        IN_game_cards.setVisibility(View.VISIBLE);
        IN_game_results.setVisibility(View.GONE);
        IN_game_start.setVisibility(View.GONE);
    }

    /**
     * show start game include and hide other includes
     */
    private void startGame() {
        IN_game_cards.setVisibility(View.GONE);
        IN_game_results.setVisibility(View.GONE);
        IN_game_start.setVisibility(View.VISIBLE);
    }

    /**
     * do request to api go to next task.
     */
    private void nextTask() {
        if (currentTask.length() > 0) {
            StringRequest nextTask = volleyHelper.nextTask(prefs.getInt("user_id", 0), session_code, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("x", response.toString());
                    try {
                        JSONObject json = new JSONObject(response.toString());
                        if (json.getString("status").equals("Next task asignt")) {
                            if (currentTask.length() > 0) {
                                //if next task is assign show wait text in the result fragment and clear the result list
                                userValueAdapter.clearData();
                            }
                        } else {
                            Log.d("next task", "no more tasks, please add one");
                            //@todo give error there is no more tasks

                            new AlertDialog.Builder(getContext())
                                    .setTitle(R.string.no_task_error_title)
                                    .setMessage(R.string.no_task_error_message)
                                    .setNeutralButton(R.string.no_task_error_button, null)
                                    .show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
            volleyHelper.addToRequestQueue(nextTask);
        }
    }

    /**
     * receive google cloud messages
     */
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Get extra data included in the Intent
            String message = intent.getStringExtra("session");

            try {
                JSONObject json = new JSONObject(message);

                ProcessCurrentTask(json);

                //update userlist for result screen
                usersList = null;
                Gson gson = new Gson();
                usersList = gson.fromJson(json.getJSONArray("user").toString(), User[].class);

                String currentCard = null;

                //check if user have an empty card and show cards if user has no cards
                if (!json.isNull("current_task")) {
                    for (User user : usersList) {

                        //check if all cards is equal
                        if (currentCard == null) {
                            currentCard = user.card;
                        } else {
                            if (currentCard.equals(user.card)) {
                                BNextTask.setVisibility(View.VISIBLE);
                            } else {
                                BNextTask.setVisibility(View.GONE);
                            }
                        }

                        if (user.getID() == prefs.getInt("user_id", 0)) {
                            if (user.card == null) {
                                //current user has no card, show card!
                                showCards();
                            }
                        }
                    }
                }

                //get current task from data

                updateList();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * update user value list from result fragment
     */
    private void updateList() {
        userValueAdapter = new UserValueAdapter(usersList);
        RVUsers.setAdapter(userValueAdapter);
    }

}
