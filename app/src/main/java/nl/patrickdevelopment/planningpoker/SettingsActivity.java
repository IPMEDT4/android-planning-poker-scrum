package nl.patrickdevelopment.planningpoker;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Response;

import org.json.JSONException;
import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Volley.JsonObjectPostRequest;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

public class SettingsActivity extends AppCompatActivity {

    Button BSave;
    EditText ETUsername;
    SharedPreferences prefs;
    VolleyHelper volleyHelper;
    private ProgressDialog progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        //
        // get prefs an volley
        //
        prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);
        volleyHelper = VolleyHelper.getInstance(getApplicationContext());
        VolleyHelper.setDefaultDialogErrorListener(SettingsActivity.this);


        //
        // set view objects
        //
        BSave = (Button) findViewById(R.id.BSave);
        ETUsername = (EditText) findViewById(R.id.ETUsername);


        //
        // set button color to white
        //
        BSave.setTextColor(getResources().getColor(R.color.colorButtonText));
        //set username from settings
        ETUsername.setText(prefs.getString("username", null));

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.LOLLIPOP){
            // Change button to ripple for lollipop and above versions
            BSave.setBackgroundResource(R.drawable.ripple);
        } else{
            // do nothing for phones running an SDK before lollipop
        }

        //
        // onclick
        //
        BSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //update iusername if username is longer than 0
                if (ETUsername.length() > 0) {

                    prefs.edit().putString("username", ETUsername.getText().toString()).apply();
                    Snackbar snackbar = Snackbar
                            .make(v, getString(R.string.savedUsernameMessage), Snackbar.LENGTH_SHORT);
                    snackbar.show();


                    //@TODO wait for google cloud messaging registering
                    //@TODO hide progress dialog by error

                    //show progress bar
                    progress = ProgressDialog.show(SettingsActivity.this, getString(R.string.progress_create_account_title),
                            getString(R.string.progress_create_account_message), true);


                    JsonObjectPostRequest request = volleyHelper.createUser(ETUsername.getText().toString(), prefs.getString("gcmToken", null), new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            //hide progress bar
                            progress.dismiss();

                            Integer user_id = 0;
                            try {
                                //get user_id
                                user_id = response.getInt("id");
                                prefs.edit().putInt("user_id", user_id).apply();

                                //introduction and account registration complete
                                prefs.edit().putBoolean("init", false).apply();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            finish();
                        }
                    });
                    volleyHelper.addToRequestQueue(request);


                } else {
                    ETUsername.setError(getString(R.string.errorUsernameMessage));
                }

            }
        });

    }

}
