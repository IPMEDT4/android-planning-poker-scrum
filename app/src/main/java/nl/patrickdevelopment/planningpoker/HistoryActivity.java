package nl.patrickdevelopment.planningpoker;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import nl.patrickdevelopment.planningpoker.Adapters.SessionAdapter;
import nl.patrickdevelopment.planningpoker.Model.Session;
import nl.patrickdevelopment.planningpoker.Volley.VolleyHelper;

public class HistoryActivity extends AppCompatActivity {

    Session[] SessionList;
    SessionAdapter sessionAdapter;
    RecyclerView RVSession;
    protected Context context;

    public HistoryActivity () {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //
        // get settings
        //

        final SharedPreferences prefs = this.getSharedPreferences(
                "nl.patrickdevelopment.planningpoker", Context.MODE_PRIVATE);

        //
        // view elements
        //
        RVSession = (RecyclerView) findViewById(R.id.RVSession);
        RVSession.setHasFixedSize(true);
        RVSession.setLayoutManager(new LinearLayoutManager(HistoryActivity.this));
        RVSession.addOnItemTouchListener(
                new RecyclerItemClickListener(getApplicationContext(), new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Intent taskHistory = new Intent(HistoryActivity.this, HistoryTaskActivity.class);
                        taskHistory.putExtra("session_id", SessionList[position].getId());
                        startActivity(taskHistory);
                    }
                })
        );

        //
        // get sessions
        //

        VolleyHelper volleyHelper = VolleyHelper.getInstance(getApplicationContext());
        JsonObjectRequest request = volleyHelper.getHistory(prefs.getInt("user_id", 0), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JSONObject json = null;
                try {
                    json = new JSONObject(response.toString());
                    SessionList = null;
                    Gson gson = new Gson();
                    SessionList = gson.fromJson(json.getJSONArray("session").toString(), Session[].class);

                    sessionAdapter = new SessionAdapter(SessionList, HistoryActivity.this);
                    RVSession.setAdapter(sessionAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

        volleyHelper.addToRequestQueue(request);
    }

}
